from django.conf.urls import patterns, url

from rest_framework.urlpatterns import format_suffix_patterns

from bookreview import views

urlpatterns = patterns(
    '',
    url(r'^$', views.index_view, name='index_view'),
    url(r'^authors/$', views.AuthorView.as_view(), name='author-list'),
    url(r'^authors/\?format=json', views.AuthorView.as_view(), name='author-list-json'),
    url(r'^authors/(?P<pk>[\d]+)/$', views.AuthorInstanceView.as_view(), name='author-instance'),
    url(r'^authors/(?P<pk>[\d]+)/\?format=json', views.AuthorInstanceView.as_view(), name='author-instance-json'),
    url(r'^json$', views.json_view, name='json_view')
)

urlpatterns = format_suffix_patterns(urlpatterns)