from django.shortcuts import render
from rest_framework import generics

import json, time, uuid
from datetime import datetime, timedelta
import random
from django.http import HttpResponse

from bookreview.models import Author
from bookreview.serializers import AuthorSerializer

def index_view(request):
    """
    Ensure the user can only see their own profiles.
    """
    response = {
        'authors': Author.objects.all(),
        # 'books': Book.objects.all(),
    }
    return render(request, 'index.html', response)

def json_view(request):

    rand_choice = ['FC Barcelona', 'Real Madrid', 'Manchester United', 'Arsenal Londyn']

    timeSleep = request.GET.get('timeSleep', 0)
    iterations = request.GET.get('iterations', 10)

    response_data = {}
    for id in range(0, int(iterations)):
        response_data[id] = {}
        response_data[id]['id'] = str(uuid.uuid4())
        response_data[id]['dateTime'] = (datetime.now() + timedelta(hours=id)).strftime('%Y-%m-%d %H:%M:%S')
        response_data[id]['matchDetails'] = {}
        response_data[id]['matchDetails']['contestant1'] = random.choice(rand_choice)
        response_data[id]['matchDetails']['contestant2'] = random.choice(rand_choice)
        response_data[id]['matchDetails']['score1'] = random.randint(0,10)
        response_data[id]['matchDetails']['score2'] = random.randint(0,10)
    time.sleep(int(timeSleep))
    return HttpResponse(json.dumps(response_data), content_type="application/json")

class AuthorView(generics.ListAPIView):
    """
    Returns a list of all authors.
    """
    model = Author
    serializer_class = AuthorSerializer

class AuthorInstanceView(generics.RetrieveAPIView):
    """
    Returns a single author.
    Also allows updating and deleting
    """
    model = Author
    serializer_class = AuthorSerializer